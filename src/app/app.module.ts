import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './Components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppModel } from '../app/Models/user-model.model';
import { DaschsboardComponent } from './Components/daschsboard/daschsboard.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button'; 
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { RegisterContactComponent } from './Components/register-contact/register-contact.component';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { NewUserComponent } from './Components/new-user/new-user.component';


@NgModule({
  declarations: [AppComponent, LoginComponent, DaschsboardComponent, RegisterContactComponent, NewUserComponent],
  imports: [
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [AppModel],
  bootstrap: [AppComponent],
})
export class AppModule {}
