export class UserModel {
  'id': Number;
  'email': String;
  'pass': String;
  'score': Number;
  'nombres': String;
  'apellidos'?: String;
  'celular': String;
  'state': Boolean;
}

export class ContactsModel {
  'id': String;
  'nombre': String;
  'apellidos': String;
  'compania': String;
  'telefono': String;
  'notas': String;
  'correo': String;
  'genero': String;
}

export class AppModel {
  contacsModel: ContactsModel = new ContactsModel();
  userModel: UserModel = new UserModel();
}
