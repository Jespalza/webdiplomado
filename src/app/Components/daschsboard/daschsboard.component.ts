import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of, Subject } from 'rxjs';
import { AppModel, ContactsModel } from 'src/app/Models/user-model.model';
import { ServiceService } from 'src/app/Services/service.service';
import { RegisterContactComponent } from '../register-contact/register-contact.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-daschsboard',
  templateUrl: './daschsboard.component.html',
  styleUrls: ['./daschsboard.component.css'],
})
export class DaschsboardComponent implements OnInit {
  listContact$: Subject<ContactsModel[]>;
  lista = [];
  constructor(
    private api: ServiceService,
    private dialog: MatDialog,
    private router: Router
  ) {

    this.listContact$ = new Subject();
  }

  ngOnInit(): void {
    if (!localStorage.getItem("UserData")) {
      this.router.navigate(['/'])
    }
    this.getContacts().subscribe((data) => {
      this.lista = data;
      console.log(this.lista);
    });
  }

  getContacts(): Observable<ContactsModel[]> {
    this.api.GetContacsUser().subscribe((data) => {
      if (data) {
        this.listContact$.next(data);
        localStorage.setItem('contacts', JSON.stringify(data));
      }
    });
    return this.listContact$.asObservable();
  }

  createContac() {
    const register = this.dialog.open(RegisterContactComponent);
    register.afterClosed().subscribe((result) => {
      if (result) {
        this.getContacts();
        Swal.fire({
          title: 'Exito!',
          text: 'Contacto registrado',
          icon: 'success',
          confirmButtonText: 'Ok',
        });
      }
      console.log(`Dialog result: ${result}`);
    });
  }
  deleteContact(iduser: String) {
    this.api.Get(`delectContact/${iduser}`).subscribe((data) => {
      this.getContacts().subscribe((data) => {
        this.lista = data;
        console.log(this.lista);
        Swal.fire({
          title: 'Exito!',
          text: 'Usuario eliminado',
          icon: 'info',
          confirmButtonText: 'Ok',
        });
      });
    });
  }
  updateContact(data) {
    const edit = this.dialog.open(RegisterContactComponent, {
      data,
    });
    edit.afterClosed().subscribe((result) => {
      if (result) {
        this.getContacts();
        Swal.fire({
          title: 'Exito!',
          text: 'Usuario modificado',
          icon: 'success',
          confirmButtonText: 'Ok',
        });
      }
    });
  }

  salir() {
    localStorage.clear();
    this.router.navigate(['/'])
  }
}
