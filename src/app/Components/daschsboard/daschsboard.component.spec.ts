import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DaschsboardComponent } from './daschsboard.component';

describe('DaschsboardComponent', () => {
  let component: DaschsboardComponent;
  let fixture: ComponentFixture<DaschsboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DaschsboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DaschsboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
