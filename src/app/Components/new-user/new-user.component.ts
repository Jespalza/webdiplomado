import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterContactComponent } from '../register-contact/register-contact.component';
import { ServiceService } from 'src/app/Services/service.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserModel } from 'src/app/Models/user-model.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  formNuevo: FormGroup;
  labelBoton = 'Registrar';
  constructor(
    private formBuilder: FormBuilder,
    // @Optional() @Inject(MAT_DIALOG_DATA) public data: UserModel,
    // private formBuilder: FormBuilder,
    private api: ServiceService,
    private dialogRef: MatDialogRef<NewUserComponent>
  ) { }

  ngOnInit(): void {
    this.formNuevo = this.formBuilder.group({
      nombres: [null, Validators.required],
      apellidos: [null, Validators.required],
      email: [
        null,
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      celular: [null, Validators.required],
      pass: [null, Validators.required],
    });
  }

  saveUser() {
    this.api.Post("register",this.formNuevo.value).subscribe(data=>{
      console.log(data);
      if(data["id"]){
        Swal.fire({
          title: 'Exito!',
          text: 'Se creo el usuario correctamente',
          icon: 'success',
          confirmButtonText: 'Cool',
        });
        this.dialogRef.close(true);
      }else{
        Swal.fire({
          title: 'Error!',
          text: 'Error al crear el usuario intentelo nuevamente',
          icon: 'error',
          confirmButtonText: 'Cool',
        });
      }
      
    })
  }

}
