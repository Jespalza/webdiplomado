import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/Services/service.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ContactsModel } from 'src/app/Models/user-model.model';

@Component({
  selector: 'app-register-contact',
  templateUrl: './register-contact.component.html',
  styleUrls: ['./register-contact.component.css'],
})
export class RegisterContactComponent implements OnInit {
  formRegistro: FormGroup;
  laberBoton = 'Guardar';
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: ContactsModel,
    private formBuilder: FormBuilder,
    private api: ServiceService,
    private dialogRef: MatDialogRef<RegisterContactComponent>
  ) {
    console.log(data);
    if (data) {
      this.laberBoton = 'Actualizar';
    }
  }

  ngOnInit(): void {
    const user = JSON.parse(localStorage.getItem('contacts'));
    if (this.data) {
      this.formRegistro = this.formBuilder.group({
        nombre: [this.data.nombre, Validators.required],
        apellidos: [this.data.apellidos, Validators.required],
        compania: [this.data.compania],
        notas: [this.data.notas],
        correo: [
          this.data.correo,
          [
            Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
          ],
        ],
        telefono: [this.data.telefono, Validators.required],
        genero: [this.data.genero, Validators.required],
      });
    } else {
      this.formRegistro = this.formBuilder.group({
        nombre: [null, Validators.required],
        apellidos: [null, Validators.required],
        compania: [null],
        notas: [null],
        correo: [
          null,
          [
            Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
          ],
        ],
        telefono: [null, Validators.required],
        genero: [null, Validators.required],
      });
    }
  }

  saveContact() {
    const user = JSON.parse(localStorage.getItem('UserData'));
    this.formRegistro.value.idUsuario = user['id'];
    let accion = 'registerContact';
    if (this.data) {
      this.formRegistro.value.id = this.data.id;
      accion = 'editContact';
    }

    this.api.Post(accion, this.formRegistro.value).subscribe((data) => {
      console.log(data);
      this.dialogRef.close(true);
    });
  }
}
