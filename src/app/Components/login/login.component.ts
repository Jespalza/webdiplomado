import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../Services/service.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NewUserComponent } from "../new-user/new-user.component";

import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  usuario = '';
  contrasena = '';
  constructor(private api: ServiceService, private router: Router,private dialog: MatDialog) {}

  ngOnInit(): void {}

  login() {
    console.log(this.usuario, this.contrasena);
    if (this.usuario != '' && this.contrasena != '') {
      this.api
        .Get(`getUser/${this.usuario}/${this.contrasena}`)
        .subscribe((data) => {
          if (data) {
            console.log(data);
            localStorage.setItem("UserData",JSON.stringify(data))
            this.router.navigate(['/daschsboard'])
          } else {
            Swal.fire({
              title: 'Error!',
              text: 'Usuario o Contraseña incorrecta',
              icon: 'error',
              confirmButtonText: 'Cool',
            });
          }
        });
    } else {
      Swal.fire({
        title: 'Error!',
        text: 'Usuario o Contraseña requeridas',
        icon: 'error',
        confirmButtonText: 'Cool',
      });
    }
  }

  registar(){
    const register = this.dialog.open(NewUserComponent);
    register.afterClosed().subscribe((result) => {
      if (result) {
        // this.getContacts();
        // Swal.fire({
        //   title: 'Exito!',
        //   text: 'Usuario registrado',
        //   icon: 'success',
        //   confirmButtonText: 'Ok',
        // });
      }
      console.log(`Dialog result: ${result}`);
    });


    // this.api.Post("register",{
    //   email
    //   pass
    //   nombres
    //   apellidos
    //   celular
    //   state
    // })
  }
}
