import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DaschsboardComponent } from './Components/daschsboard/daschsboard.component';
import { LoginComponent } from './Components/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'daschsboard',
    component: DaschsboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
