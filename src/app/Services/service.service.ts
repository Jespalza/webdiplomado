import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactsModel, UserModel } from '../Models/user-model.model';
import { AppModel } from '../Models/user-model.model';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  listContact$: Subject<ContactsModel[]>;
  contacts: ContactsModel[] = [];
  constructor(private http: HttpClient, private appModel: AppModel) {
    this.listContact$ = new Subject();
  }

  Post(action: String, json: {}) {
    var url = 'http://localhost:8082/' + action;
    return this.http.post(url, json);
  }

  Get(action: String) {
    var url = 'http://localhost:8082/' + action;
    return this.http.get(url);
  }

  GetContacsUser() {
    var user = JSON.parse(localStorage.getItem('UserData') || '{}');
    var url = `http://localhost:8082/contacsUser/${user['id']}`;
    return this.http.get<ContactsModel[]>(url);
  }
}
