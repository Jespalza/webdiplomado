package _com3.api.services.repository;

import _com3.api.messages.contactRequest;
import _com3.api.model.contacModel;
import _com3.api.model.userModel;
import java.util.List;

public interface CrudRepository {
  public userModel findUserForEmail(String email, String pass);

  public userModel registerUser(userModel user);

  public List<contacModel> findContactsUser(Integer idUser);

  public contacModel deleteContact(Long IdContact);

  public contacModel registerContact(contacModel contacts);

  public contacModel editContact(contacModel contact);
}
