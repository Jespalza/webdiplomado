package _com3.api.services.dataBase;

import _com3.api.messages.contactRequest;
import _com3.api.messages.contactResponse;
import _com3.api.model.contacModel;
import _com3.api.model.userModel;
import _com3.api.services.Interfaces.IContactsUser;
import _com3.api.services.Interfaces.IUserService;
import _com3.api.services.repository.CrudRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CrudDBRepository implements CrudRepository {

  private final IUserService iUserService;
  private final IContactsUser iContactsUser;

  public CrudDBRepository(
    IUserService iUserService,
    IContactsUser iContactsUser
  ) {
    this.iUserService = iUserService;
    this.iContactsUser = iContactsUser;
  }

  @Override
  public userModel findUserForEmail(String email, String pass) {
    List<userModel> listUsers = this.iUserService.findAll();
    userModel user = listUsers
      .stream()
      .filter(
        fil ->
          fil.getEmail().startsWith(email) && fil.getPass().startsWith(pass)
      )
      .findFirst()
      .orElse(null);

    return user;
  }

  @Override
  public userModel registerUser(userModel user) {
    return this.iUserService.save(user);
  }

  @Override
  public List<contacModel> findContactsUser(Integer idUser) {
    List<contacModel> listContacts =
      this.iContactsUser.findAll()
        .stream()
        .filter(fil -> fil.getIdUsuario() == idUser)
        .collect(Collectors.toList());
    return listContacts;
  }

  @Override
  public contacModel deleteContact(Long IdContact) {
    this.iContactsUser.deleteById(IdContact);
    // return new contactResponse("prueba", HttpStatus.OK);
    return null;
  }

  @Override
  public contacModel registerContact(contacModel contact) {
    return this.iContactsUser.save(contact);
  }

  @Override
  public contacModel editContact(contacModel contact) {
    this.iContactsUser.deleteById(contact.getId());

    return this.iContactsUser.save(contact);
  }
}
