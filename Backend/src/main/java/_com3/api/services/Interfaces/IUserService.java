package _com3.api.services.Interfaces;

import _com3.api.model.userModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserService extends JpaRepository<userModel, String> {}
