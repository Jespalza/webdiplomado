package _com3.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contactos")
public class contacModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "Nombre")
  private String Nombre;

  @Column(name = "Apellidos")
  private String Apellidos;

  @Column(name = "Compania")
  private String Compania;

  @Column(name = "Telefono")
  private String Telefono;

  @Column(name = "Notas")
  private String Notas;

  @Column(name = "Correo")
  private String Correo;

  @Column(name = "genero")
  private Boolean genero;

  @Column(name = "id_usuario")
  private Integer idUsuario;

  public Integer getIdUsuario() {
    return idUsuario;
  }

  public void setIdUsuario(Integer idUsuario) {
    this.idUsuario = idUsuario;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNombre() {
    return Nombre;
  }

  public void setNombre(String nombre) {
    Nombre = nombre;
  }

  public String getApellidos() {
    return Apellidos;
  }

  public void setApellidos(String apellidos) {
    Apellidos = apellidos;
  }

  public String getCompania() {
    return Compania;
  }

  public void setCompania(String compania) {
    Compania = compania;
  }

  public String getTelefono() {
    return Telefono;
  }

  public void setTelefono(String telefono) {
    Telefono = telefono;
  }

  public String getNotas() {
    return Notas;
  }

  public void setNotas(String notas) {
    Notas = notas;
  }

  public String getCorreo() {
    return Correo;
  }

  public void setCorreo(String correo) {
    Correo = correo;
  }

  public Boolean getGenero() {
    return genero;
  }

  public void setGenero(Boolean genero) {
    this.genero = genero;
  }
}
