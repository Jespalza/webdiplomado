package _com3.api.messages;

public class contactResponse {

  private long id;
  private String Nombre;
  private String Apellidos;
  private String Compania;
  private String Telefono;
  private String Notas;
  private String Correo;
  private Boolean genero;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNombre() {
    return Nombre;
  }

  public void setNombre(String nombre) {
    Nombre = nombre;
  }

  public String getApellidos() {
    return Apellidos;
  }

  public void setApellidos(String apellidos) {
    Apellidos = apellidos;
  }

  public String getCompania() {
    return Compania;
  }

  public void setCompania(String compania) {
    Compania = compania;
  }

  public String getTelefono() {
    return Telefono;
  }

  public void setTelefono(String telefono) {
    Telefono = telefono;
  }

  public String getNotas() {
    return Notas;
  }

  public void setNotas(String notas) {
    Notas = notas;
  }

  public String getCorreo() {
    return Correo;
  }

  public void setCorreo(String correo) {
    Correo = correo;
  }

  public Boolean getGenero() {
    return genero;
  }

  public void setGenero(Boolean genero) {
    this.genero = genero;
  }
}
