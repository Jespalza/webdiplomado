package _com3.api.messages;

public class userResquest {
    private long id;
    private String email;
    private String pass;
    private String nombres;
    private String apellidos;
    private String celular;
    private boolean state;
  
    public long getId() {
      return id;
    }
  
    public void setId(long id) {
      this.id = id;
    }
  
    public String getEmail() {
      return email;
    }
  
    public void setEmail(String email) {
      this.email = email;
    }
  
    public String getPass() {
      return pass;
    }
  
    public void setPass(String pass) {
      this.pass = pass;
    }
    
    public String getNombres() {
      return nombres;
    }
  
    public void setNombres(String nombres) {
      this.nombres = nombres;
    }
  
    public String getApellidos() {
      return apellidos;
    }
  
    public void setApellidos(String apellidos) {
      this.apellidos = apellidos;
    }
  
    public String getCelular() {
      return celular;
    }
  
    public void setCelular(String celular) {
      this.celular = celular;
    }
  
    public boolean isState() {
      return state;
    }
  
    public void setState(boolean state) {
      this.state = state;
    }
}
