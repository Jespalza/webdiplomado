package _com3.api.controller;

import _com3.api.messages.contactRequest;
import _com3.api.messages.userResquest;
import _com3.api.model.contacModel;
import _com3.api.model.userModel;
import _com3.api.services.dataBase.CrudDBRepository;
import com.google.gson.Gson;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class UserController {

  @Autowired
  private CrudDBRepository crudDBRepository;

  @GetMapping("/getUser/{email}/{pass}")
  public userModel listUser(
    @PathVariable String email,
    @PathVariable String pass
  ) {
    userModel user = crudDBRepository.findUserForEmail(email, pass);
    return user;
  }

  @PostMapping(
    path = "/register",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public userModel registerUser(@RequestBody userResquest userReq) {
    userModel userDb = new userModel();
    userDb.setApellidos(userReq.getApellidos());
    userDb.setNombres(userReq.getNombres());
    userDb.setEmail(userReq.getEmail());
    userDb.setCelular(userReq.getCelular());
    userDb.setPass(userReq.getPass());
    userDb.setState(true);

    return crudDBRepository.registerUser(userDb);
  }

  @PostMapping(
    path = "/registerContact",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> registerContact(
    @RequestBody contacModel contact
  ) {
    var response = crudDBRepository.registerContact(contact);
    if (response != null) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  @PostMapping(
    path = "/editContact",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public contacModel editContact(@RequestBody contacModel contact) {
    contacModel response = crudDBRepository.registerContact(contact);
    return response;
  }

  @GetMapping("/contacsUser/{iduser}")
  public List<contacModel> listContacUser(@PathVariable Integer iduser) {
    List<contacModel> listContacts = crudDBRepository.findContactsUser(iduser);
    return listContacts;
  }

  
  @GetMapping("/delectContact/{idcontact}")
  public ResponseEntity<String> deleteContacUser(@PathVariable Long idcontact) {
    crudDBRepository.deleteContact(idcontact);
    return new ResponseEntity<String>(
      new Gson().toJson(idcontact),
      HttpStatus.OK
    );
  }
}
